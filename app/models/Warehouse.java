package models;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;
@Entity
public class Warehouse extends Model {
    public static Finder<Long, Warehouse> find = new Finder<>(Long.class, Warehouse.class);
    @Id
    public Long id;
 
    public String name;
 
    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList();

    @OneToOne
    public Address address;
 
    public String toString() {
        return name;
    }
}