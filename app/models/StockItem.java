package models;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class StockItem extends Model{
    public static Finder<Long, StockItem> find = new Finder<>(Long.class, StockItem.class);

    @Id
    public Long id;

	 @ManyToOne
  	public Warehouse warehouse;
  	
  	@ManyToOne
  	public Product product;
  	
  	public Long quantity;

    public String toString() {
        return String.format("StockItem %d - %d x product %s",
                id, quantity, product == null ? null : product.id);
    }
}