package models;

import java.util.ArrayList;
import java.util.*;
import java.util.List;
import java.util.LinkedList;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class Tag extends Model{
    public static Finder<Long, Tag> find = new Finder<>(Long.class, Tag.class);
    
    public static Tag findById(Long id) {
      return find.byId(id);
    }

    @Id
    public Long id;
    
    @Constraints.Required
    public String name;
 
    public Tag(){
        // Left empty
    }
    
    public Tag(Long id, String name, Collection<Product> products) {
        this.id = id;
        this.name = name;
        this.products = new LinkedList<Product>(products);
        for (Product product : products) {
            product.tags.add(this);
        }
    }

    @ManyToMany(mappedBy="tags")
    public List<Product> products = new LinkedList<Product>();
    

}