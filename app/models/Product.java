package models;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;

import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class Product extends Model implements play.mvc.PathBindable<Product>{
    @Id
    public Long id;

    @Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }
 
    @Override
    public String unbind(String key) {
        return ean;
    }
 
    @Override
    public String javascriptUnbind() {
        return ean;
    }

    @ManyToMany
    public List<Tag> tags = new LinkedList<Tag>();

    @Constraints.Required
    public String ean;
  
    @Constraints.Required
    public String name;
  
    public String description;
    public byte[] picture;

    @OneToMany(mappedBy="product")
    public List<StockItem> stockItems;


    //Lấy dữ liệu trong database
    public static Finder<Long,Product> find = new Finder<Long,Product>(Long.class, Product.class);
 
    public Product() {}
    public Product(String ean, String name, String description) {
        this.ean = ean;
        this.name = name;
        this.description = description;
    }
    public String toString() {
        return String.format("%s - %s", ean, name);
     }

    private static List<Product> products = new ArrayList<Product>();


    public static List<Product> findAll() {
        return find.all();
    }
 
    public static Product findByEan(String ean) {
        return find.where().eq("ean", ean).findUnique();
    }
 
    public static List<Product> findByName(String term) {
        final List<Product> results = new ArrayList<Product>();
        for (Product candidate : products) {
            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return results;
    }

    public static Page<Product> find(int page) {  // trả về trang thay vì List
        return find.where()
                .orderBy("id asc")     // sắp xếp tăng dần theo id
                .findPagingList(10)    // quy định kích thước của trang
                .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
                .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
    }
}