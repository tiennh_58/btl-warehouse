package controllers;
import models.*;
import java.util.List;
import java.util.ArrayList;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.products.list;
import views.html.products.details;
import play.data.Form;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import play.mvc.Security;

@Security.Authenticated(Secured.class)
public class Products extends Controller{
	private static final Form<Product> productForm = Form.form(Product.class);

    public static Result list(Integer page) {
        Page<Product> products = Product.find(page);
        return ok(views.html.catalog.render(products));
    }

	public static Result newProduct(){
		return ok(details.render(productForm));
	}

	public static Result details(Product product) {
        if (product == null) {
            return notFound(String.format("Product %s does not exist.",product.ean));
        }
        Form<Product> filledForm = productForm.fill(product);
        return ok(details.render(filledForm));
    }

	public static Result save(){
		Form<Product> boundForm = productForm.bindFromRequest();
  		if (boundForm.hasErrors()) {
    		flash("error", "Please correct the form below.");
    		return badRequest(details.render(boundForm));
  		}
  		Product product = boundForm.get();
    List<Tag> tags = new ArrayList<Tag>();
    for (Tag tag : product.tags) {
      if (tag.id != null) {
        tags.add(Tag.findById(tag.id));
      }
    }
    product.tags = tags;
    
    StockItem stockItem = new StockItem();
    stockItem.product = product;
    stockItem.quantity = 0L;
    stockItem.save();

    if(product.id == null) Ebean.save(product);
    else product.update();
  	stockItem.save();

    flash("success", String.format("Successfully added product %s", product));
  	return redirect(routes.Products.list(0));
	}

  public static Result delete(String ean) {
    final Product product = Product.findByEan(ean);
    if(product == null) {
      return notFound(String.format("Product %s does not exists.", ean));
    }
    for (StockItem stockItem : product.stockItems) {
            stockItem.delete();
        }
    product.delete();
    return redirect(routes.Products.list(0));
  }
}